use std::{
    ffi::OsStr,
    ops::Deref,
    path::{Path, PathBuf},
};

use anyhow::Result;

use crate::util;

const BUILD_DIR_NAME: &str = "BUILD";
const BUILDROOT_DIR_NAME: &str = "BUILDROOT";
const RPMS_DIR_NAME: &str = "RPMS";
const SOURCES_DIR_NAME: &str = "SOURCES";
const SPECS_DIR_NAME: &str = "SPECS";
const SRPMS_DIR_NAME: &str = "SRPMS";

#[derive(Debug, Clone)]
pub struct PackageBuildRoot {
    pub path: PathBuf,
    pub build: PathBuf,
    pub buildroot: PathBuf,
    pub rpms: PathBuf,
    pub sources: PathBuf,
    pub specs: PathBuf,
    pub srpms: PathBuf,
}

impl PackageBuildRoot {
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self> {
        let path = path.as_ref().to_path_buf();
        let build = path.join(BUILD_DIR_NAME);
        let buildroot = path.join(BUILDROOT_DIR_NAME);
        let rpms = path.join(RPMS_DIR_NAME);
        let sources = path.join(SOURCES_DIR_NAME);
        let specs = path.join(SPECS_DIR_NAME);
        let srpms = path.join(SRPMS_DIR_NAME);

        util::create_dir_all(&path)?;
        util::create_dir_all(&build)?;
        util::create_dir_all(&buildroot)?;
        util::create_dir_all(&rpms)?;
        util::create_dir_all(&sources)?;
        util::create_dir_all(&specs)?;
        util::create_dir_all(&srpms)?;

        Ok(Self {
            path,
            build,
            buildroot,
            rpms,
            sources,
            specs,
            srpms,
        })
    }
}

impl Deref for PackageBuildRoot {
    type Target = Path;

    fn deref(&self) -> &Self::Target {
        &self.path
    }
}

impl AsRef<OsStr> for PackageBuildRoot {
    fn as_ref(&self) -> &OsStr {
        self.as_os_str()
    }
}
